---
layout: post
title: Double Bill
publish_date: 2024-10-28T14:15:40.550Z
author: Alexandre Galmard
---
Thoughts from watching *[Night is Limpid](https://quantitycinema.com/night-is-limpid)* & *[He Thought He Died](https://quantitycinema.com/he-thought-he-died)* on July 29, 2024.

Starting a new film is a split, a line on a spreadsheet: Thinking, being. Making history your own to share or to cut out, to delete and clear the space of deletion, requires a place for the new and a forcing out of old clusters—masterpieces and trash at the tip of our fingers, both amidst what “a new order of the day” has convened.

![](/assets/uploads/criticism_ag_db01.png)

![](/assets/uploads/criticism_ag_db02.png)

Poincaré famously said that mathematics is the art of giving the same name to different things. I never really understood what that meant until I started giving the same name to different things, by saying: this is cinema, this is so cinema. It’s a simple thing to be able to call some thing with the same name as the whole thing, and for it to be just as worthy as what it is standing apart from. Which is why we love movies the same way that we love going to them, filled with such permanent possibilities, ready to be boxed or unboxed. Ideas entertain their own structures, and these operating models sustain the forms required to structure such an entertainment.

*Night is Limpid*, by thinking thinking, is written like it is spoken. You are not watching slices of life, of life being lived, you are watching thinking being thought, albeit a time and space of a different kind, or rather, one of an embodied ideation, one which no longer discerns life and ideas, where life itself is a live action of ideas. This produces investigative narratives integrated as essential parts of being, creating a sense of speaking-to-oneself across many tones, like an open monologue reflecting “inside one’s own mind”. *He Thought He Died* refracts against its surroundings—like encounters made to leave us speaking to and quoting ourselves anew. Here, thinking thinking has little to do with an anti-conformist folklore, on the contrary: cutting to an opposite turns an edge into a new middle, a new center, and ultimately a new average. From one to the next, fibers become the tightly woven fabric of a unique cloth.

![](/assets/uploads/criticism_ag_db03.png)

![](/assets/uploads/criticism_ag_db04.png)

Writing a piece of music is in itself already making music, the writing presupposes its existence, as such, it concretely exists in some world at least, even if not immediately it’s own. And poetry is not meant to be acted or instantly adapted but immediately plays as it is read. Every step is preproduction for the next thing coming. If *Night is Limpid* tells us that being does not let itself be written, that there is always some sort of lack, and *He Thought He Died* states that thinking does not let itself be directed, that it is always in some sort of excess, that thought spills, then making movies is perhaps stringing together, at some point in time, these two impossibilities. Once “you leave the phase of your early work; and that “you have all the subjects of your final works”, you are free to catch up to yourself, because it goes both ways in this sense. By slowly building yourself into a museum and walk in it so to speak, you are paving the way for creation to appear as its own—enough so that you can re-encounter yourself within your own projections, where all of history’s depths don’t seem like two mirrors bouncing off each other, but two movies playing back to back.

You don’t have to write enormous background stories to your characters so they can play them more accurately, but as any operator knows, you should be able to write yours. As much as it is you, you still need to embody your own character, to "caricature and explicate” yourself better, because you’re always your first role. In this sense (even unbeknownst to them), every moviemaker becomes see-through through their own moviemaking; as they “learn \[their] desire to learn” and trust themselves to be true when making from what they’ve learned and learning from what they’ve made, to think-through and be thought-through in return.

It’s not so much that the real show lies behind the scene, that everything happens wrong outside the shot so that everything can happen right inside of it—though that is often the case. But when I see Isiah’s sets, everything warps around his ideas, I see the world bend and it’s a wonderful thing to see, because there’s a lot of it which ends up in the films themselves. There is a palpable immanence, a palpable immunity. He doesn’t force things to appear the way he desires, he renders visible what was left unrecognized within each situation, so that the subject matter of his body-of-work is its own ideal becoming, continually deriving to and unfolding itself. This is the meaning of the famous motto that artists steal, which he recalls in his [interview](https://agnes.queensu.ca/digital-publication/he-thought-he-died/?state=read): there are repetitions, resurgences and derivations, but true artists steal, they steal back from themselves, or steal themselves back from others. 

By stealing his own paintings from the museum, the painter creates a gap in history, punctures a hole in the catalog, avoids the encyclopedic registry in order to found his own perspective, his own vantage point, a *point-de-fuite*, “the closest one can get to the creation of a camera”. When Isiah paints and films, paints his own paintings and films them both, his character is retraced. The protective glass of the paintings separates him from the work while offering his own image back to an already ghastly reflection. The “[translucent](https://quantitycinema.com/2022/11/07/silver-exposure-on-night-is-limpid)” thoughts of *Night is Limpid* has been carried out onto the body, presented as a forced displacement, one that will leave nothing behind but empty frames, bleeding edges. He is like a generic extension of this obscure vault of dead artists, excrescent, and without having ever been *there* (as every successful heist demands), he remains indiscernible and unclassified, that is, subtracted from the scene and dissociated from whatever classifies it: he is not counted, nor would he have ever been if he had ever remained in this state—or to the State (Badiou, 338), that is, outside a film being made.

![](/assets/uploads/criticism_ag_db05.png)

![](/assets/uploads/criticism_ag_db06.png)

![](/assets/uploads/criticism_ag_db07.png)

All genuine thinking is free because every true invention belongs to itself, through the idea’s embodiment and our enlightened adaptation in regards to it. This is what free time looks like after invention, and the affect most closely resembles enthusiasm, that is, enthusiasm to make art, and even, to make art inevitable. I am able to understand genius, with their many exceptionalities and underlying logics being too often erased by contemporary materialisms, without believing in anything innate or given, because it all still remains to be done or seen, as minimal the degrees to instantiate. If it took Picasso four years to paint like Raphael, but a lifetime to paint like a child, that is because unlearning is another process in itself. I am reminded of Grothendieck’s rant about the primacy of innocence over some natural gifts in order to uncover the secrets of the Universe. And I’m fond of the idea that mountains may be moved by pure chance, not as essence or accident, but rather by taking simple risks of great consequences.

This should give us the confidence to not limit ourselves to one kind of making over another, to desire for new traditions and refuse commercialization and constructivist legislation altogether. Cutting diagonally across the existing types recollects them all into a traceable procedure, one which must only avoid becoming a type in itself, one which must thus most importantly preserve its virtuosity through any and all experiences of the world. This seems to be the affirmation at hand today, to think through contradiction and offer the possibility of a predication beyond cultural historicity and its many attempts to naturalize the marketplace, which Isiah commands us to seize, singularly interrogating a law which has no support, no canvas to rely on but a cover frame, or at best, a fragile protection. 

If every good painter paints what he is, then Isiah, rather than “\[obscuring] the causeless place of \[his] birth”, consistently enlightens the grounded force which brought him to this point, presenting us with the gift of genius itself, the missing link to another possible present. As sounds fade into the crushing of waves and chime-like cutting carves a shape in and of the mind, one sees politics being transposed to a new key (Clarke, 238), for subtraction is a passion, and not an identity. I simply hope to never see any more “Thank You Movie God” in my lifetime. Maybe this is Isiah’s imperative after all, with Cézanne: make the art, there lies salvation—ceaselessly reminding us that at play is first and foremost a mastery of oneself, for a said that’s been done and a done that’s been said, through to “the end”, twice.

![](/assets/uploads/criticism_ag_db08.jpg)

![](/assets/uploads/criticism_ag_db09.png)

— Alain Badiou. *Being and Event*, 1988.\
— T.J. Clarke. *Heaven On Earth: Painting and the Life to Come*, 2020.\
— Isaac Goes. Silver Exposure: On ‘Night is Limpid’, 2022.\
— Isiah Medina. Three Questions with Andrei Pora, 2024.