---
layout: post
publish_date: 2022-11-07 17:08:17 +0000
title: 'Silver Exposure: On "Night is Limpid"'
author: Isaac Goes

---
A peach and two nectarines rest on a chair in a near-vacant room, a disembodied still life arrangement hanging suspended between bare walls. In another room, heaped drop cloths smother tables and bookshelves, their outlines blurred into vague shapeless mounds. It’s as if these walls and sheets extend endlessly in every direction, an infinite backdrop divided only by the occasional painted canvas, by tubed colors tearing at the fabric of this totalizing vacuum.

This room and others like it stage our drama. Characters float between these undefined spaces, they discourse on transcendental subjectivity and the possibility of thinking without names. They edge nearer to uncovering the true shapes of the shrouded objects that surround them only to be resisted by their obscurity. Denied an object, their efforts are reflected back towards them, they receive an image not of the object beneath the cover, but of themselves attempting to think it.

Night is _limpid_, translucent yet not empty. This seeming emptiness is a container, the invisible filter through which we experience objects and events, the opening in our subjectivity that frames our thoughts and perceptions. What does it take to bring this invisible filter into focus? To trace the edges of an object we are so accustomed to seeing through that it scarcely registers as an object at all?

When we talk about continuity editing we often refer to the cuts as invisible, in the case of _Night is Limpid_ it may serve us better to speak of them as translucent. It is a film in which ideas bridge gaps in space and time, in which continuity is achieved not by invisible editing techniques but by the measured unfolding of ideas. The film’s chronology is tangled, characters begin thoughts in one room and finish them in another, voiceover weaves in and amongst diegetic dialogue. In spite of this it is seldom jarring. The ideas don’t compete with this disorder, they frame it as a text to be read.

Reversals of the translucent and the opaque, of the object of thought and its background undergird the work. We see seeing, we hear hearing, we become aware of the film behind the film. Like a painter before a landscape the arena of representation begins to take on a more defined shape for us. What we once took for the given order of nature reveals itself as an object of labor, each brushstroke a mediated encounter between thought and its object. We’ve taken a step back, the easel is no longer a window into the world but another object in our purview, the frame of our experience becomes framed itself.