---
layout: post
publish_date: 2017-04-20T02:36:57.000+00:00
title: Projecting the Prequels
author: Isiah Medina

---
![](/assets/uploads/Criticism_IM_Prequels1.png)

> The primary expression here is puzzlement, some indication that Ethan does not know his own mind and suddenly realizes he does not know his own mind…
>
> — Robert Pippin, _Hollywood Westerns and American Myth: The Importance of Howard Hawks and John Ford for Political Philosophy_

![](/assets/uploads/Criticism_IM_Prequels2.png)

![](/assets/uploads/Criticism_IM_Prequels3.png)

![](/assets/uploads/Criticism_IM_Prequels4.png)

![](/assets/uploads/Criticism_IM_Prequels5.png)

![](/assets/uploads/Criticism_IM_Prequels6.png)

![](/assets/uploads/Criticism_IM_Prequels7.png)

![](/assets/uploads/Criticism_IM_Prequels8.png)

![](/assets/uploads/Criticism_IM_Prequels9.png)

I have some friendships that began online simply because they placed two frames from two different films side by side, or one below the other, with perhaps not even a name or a title, and I liked it. Through 0s and 1s I would see the infinite play of identity and difference. In the context of years of comparing frames to other frames, for the minimal and maximal differences and identities, of finding, in its rub of fragments, new counter-histories, accomplishments, and signs of a future cinema waiting to be plucked out anew, it should have been clear that at one point we would bump into the prequels again, as the prequels, in their form, are the essence of film criticism for those with a Tumblr. Bill Krohn commented that _Star Wars_ is nothing but the continual reversal of signs, and what better form than the comparative screen capture to understand this series. Like all comparisons without writing, either you see the connection, you see the lineage or you don’t. If the comparison is trivial, the connection too loose, we may see that we are not in a thinking of montage but simply a sequence of shots. But all the comparisons work in a thinking of montage that turns the images into something both prequel and fraternal. Every true work of art re-invents the tradition it belongs to, opens up what came before it, and is the condition of what is old to persist as still new. A true work turns its predecessors into unexpected prequels.

**1.**

The transitions between scenes, the different wipes that go diagonally, turn the screen into blinds, into squares, a minute hand on the clock, become more and more pronounced as the prequels succeed one another. Indeed the prequels are a question of transition, of succession of one form of government to another. It becomes most clear in _Episode II: Attack Of The Clones_, as Neil Bahadur pointed out, the very colour of the political world changes. All I would add is it appears like a diagonal wipe that is often seen across the films, except here there is no wipe at all, the world itself is in transition, no need for the subjective jump of the actualized wipe.

![](/assets/uploads/Criticism_IM_Prequels10.png)

![](/assets/uploads/Criticism_IM_Prequels11.png)

The lack of the wipe, of the transition, does not mean there is no transition occurring. In fact, the lightsaber duel between Darth Tyranus and Anakin manifests the actuality of transition within the battles themselves: in close-up the lightsabers act like the transitioning wipes, opening or closing down the frame, the blue or the red floating atop the faces, in the heat of battle political realignment can occur. Each attack on the other is a transitional wipe that only returns one to themselves. Even the sound during this sequence emphasizes the moving of the sabers about themselves rather than the actual hitting of two sabers. Via over-anticipation of the coming hit, we touch air, a pure rotation —

![](/assets/uploads/Criticism_IM_Prequels12.png)

![](/assets/uploads/Criticism_IM_Prequels13.png)

![](/assets/uploads/Criticism_IM_Prequels14.png)

![](/assets/uploads/Criticism_IM_Prequels15.png)

_die Zuschauer sich drehen,_ rotating around itself, like a Kuleshov effect that returns to Darth Vader again and again as he looks at Skywalker, and the Emperor.

**2.**

The decision to kill the Emperor emerges through a Kuleshov effect, or to put it another way, decisions arise through projections, through the coincidence of a blank face estranged from itself. The K-effect exemplifies that one does not know their own mind, but knows this, and perhaps only this opaque blankness. What is precisely blank is that this decision, to save Luke, repeats a decision when he saves Palpatine in _Episode III_. The first time, he loves Padmé, the second time, his son. In truth the decision is the same each time, it is its own K-effect, whether one is good or one is evil is a pure projection on the same decision. Anakin is the chosen one, as he only chooses the one choice, out of love, twice. The one decision is a clone of the other, a cloning of the chosen one. If to the question rye or wheat, Verdoux says “Yes,” if Anakin is asked, good or evil, he will reply, “Yes.”

Throughout, Anakin always has a Master. He says Obi-Wan is like a father. As Darth Vader, he is almost constantly kneeling. Finally in _Episode III_, Obi-Wan says: “You were my brother Anakin. I loved you.” Neither Master nor slave, but _kin_. A figure of equality. Neither the law of the Jedi nor the desire of the Sith, but love.

_Episode III_ ends in elation as we know what happens in the later episodes. At this mid-point in the story, we are in Anakin’s position: _we see the future (which already happened)._ And what happened? The choice of love, twice. Neither beyond good, nor evil, but the risk of living through both.

He failed where others succeeded, he succeeded where others failed: he took control of the universe. Anakin turns the world into a green screen, destroying and creating the conditions to love unconditionally.

![](/assets/uploads/Criticism_IM_Prequels16.png)

To choose one, or choose zero, is the question of the digital. The genius of _Episode II: Attack of the Clones_, a landmark in digital cinema, is that it poses the question of the form of the digital against a critique of democracy. Yes (1), the form of digital offers a true choice, No (0) democracy does not offer true choices. Schelling saw that evil is more directly spiritual than the good in its cold abstract hatred of reality. The unrepentant joys of CGI are evil in this formal sense, and beautiful for the same reason. In showing the rise of evil, we need the digital imagination unfettered by the studio interference, democratic test-screenings, and the care of making a movie ‘for the fans’, the majority. In _Les trois désastres_, Jean-Luc Godard claimed that digital will be a dictatorship. If celluloid long takes were democratic in its manipulation, then the case is not that of democracy vs. dictatorship, but that dictatorship is the truth of democracy’s purely _formal_ manipulations. The democratization of the digital cinema, in terms of criticism and filmmaking, must be coupled with the digitization of democracy, the shift in making transparent all the points, decisions, zero and one, that show its formal identity to dictatorship.

At the level of the Jedi vs the Sith, in the concrete moment of decision, there is nothing to say that the Jedi are better than the Sith. The Separatists and the Republic are both headed by Palpatine, which is to say, the Republic’s war against the Separatists are an outgrowth of the divisions produced by democracy itself. As a political problem, the solution of _more_ democracy, or in another popular phrase, a _real_ democracy opposed to a fake one, is false and explains nothing. When Lucas claims that the Republic is the Empire, we have a speculative judgment, which is to say, there is only a formal change in the transition. As Palpatine says, its a point of view, and the Jedi and the Sith are alike in almost every way. The glow of the lightsaber on the face of the warrior is an instance of a K-effect with no need for an opposing shot to infuse a blankness with meaning.

Thus when Anakin makes the choice, the same choice of love, in _Episode III_ and _VI_, in fact _nothing at all is learned_. His choice was correct both times. The first choice of love destroys the law of the Jedi, and the second choice of love destroys the desire of the Sith. Love is beyond good or evil and he brings balance by destroying them both. “You were the chosen one!” Obi-Wan yells, and yet, Anakin was, always will have been, chosen, by his own choosing. The mystery of the prophecy remains a mystery to those who believe it, but not to the one who needs not to learn it. If nothing was learned, what was learned was the _nothing_ of subjectivity, to be nothing but equal to one’s choice.

**3.**

The logic of the digital continues as when Obi-Wan tells Anakin that only a Sith deals in absolutes, only to later say “Senator Palpatine is evil” to which Anakin replies “In my point of view, the Jedi are evil”. Exhausted, Obi-Wan yells “then you are lost!” Of course many commentators on this and other popular films will claim that the ideology is always inconsistent, and designed that way to attract the largest possible market. However the issue is not inconsistency itself, but _which_ inconsistency? Only the greatest philosophers and artists contradict themselves, since they approach a real point of tension that cannot be easily dissolved into the morality of their time. A true artist will create the precise contours of ideological inconsistency, and situate us at various points of impasse. Any film without this tension is perhaps neutered and of interest only for patting one self on the back for being on the right side of history. Instead, working in these tension spaces, we find that not only is the future open, but so is the past itself. After all, that is the creative struggle in creating a prequel.

![](/assets/uploads/Criticism_IM_Prequels17.png)

![](/assets/uploads/Criticism_IM_Prequels19.png)

![](/assets/uploads/Criticism_IM_Prequels20.png)

![](/assets/uploads/Criticism_IM_Prequels21.png)

![](/assets/uploads/Criticism_IM_Prequels22.png)

If Lucas’ _Star Wars_ writes the transition of one frame to another, there must be something shared to register the change from one to the other, a part of change that itself does not change. That is, we must assume an invariance that is discovered by the back and forth between one era to another. This is projection. As Daniel Morgan writes that for Godard: 1917 _is_ 1789, and so is 1848, and Weimar in 1945 is Weimar in 1806, and finally Berlin of 1944 is Nosferatu’s village of 1922, as seen by Godard in the 1950s at the Cinémathèque Française. Pedro Costa makes a similar remark when he says “There will be someone from Finland who’ll ask about Fontainhas. Fontainhas is Russia in ‘17, it’s Hollywood in ‘34. It’s not more or less than that.” Neil Bahadur, commenting on Straub-Huillet’s _Fortini/Cani_: “street scenes take on multiple meanings: 1976 Rome serves both as stand-in for 1940’s Germany, Italy, and France, reminding us how easily citizens accepted fascism \[…\], then also as 1976 Rome in relation to history - \[…\] democratic systems try to design us to ‘forget’ the past, and spaces which haven’t changed at all.” Lucas makes a similar projection. The original Star Wars was inspired by the Vietnam War, and many commentators related _Episode II_ to the Iraq War, Anakin kills the ‘sand people’, and slaughters the men, women and children, “like animals” because “they are animals” (to which Padmé comforts with a link of humanism to the justification of terror: “to be angry is to be human”). Lucas in 2005: “The parallels between what we did in Vietnam and what we’re doing in Iraq now are unbelievable.” To track the invariance of democracy becoming itself, i.e. a dictatorship, the projections of studying history become strikingly compressed into what is called a “\[fleecing\] and plunder” by Jonathan Rosenbaum: “various planets recycle the stereotypical settings, costumes, hair styles, and accents of Renaissance Venice, Africa, India, China, and the Middle East.” What is a street scene in Straub-Huillet becomes an abstraction of stereotypes of entire planets, and the legend of stereo continues where democracy projects dictatorship. Or to put it like Pedro Costa speaking on Rossellini, the degree of abstraction gets higher and higher as the prequels progress. And what is key here is that we are truly in a deadlock, and its a deadlock that is in our past, a long time ago, in a galaxy far, far away. The relation and reversibility of democracy and dictatorship is a true problem not to be solved simply by choosing one over the other as they are the same. It is democratic to have to choose one or the other, which is to say, it is a false choice, and at the same time, it is a forced choice, dictated to us. Like Stalin speaking on left and right deviations, he claims that they are both worse. Anakin finds that both the Jedi and Sith are both worse, opening the space for another possibility. I don’t see Luke as a Jedi, as both Obi-Wan and Yoda claimed that he must kill/confront Vader to become one, and thus both the legacies of the Sith and Jedi die with Anakin. What we are left with, in the final shot, are a set of different species with the false forced choices of Jedi and Sith no longer operative, just a generic set with in an open space of perhaps new decisions, where even the Jedi who show up as immortal ghosts are ambiguous in their consequences, as it is a desire of the Sith to be immortal.

**4.**

Self-abolishment would be the true solution, a world without either Sith nor Jedi. The passage of a Tramp to Hynkel to Verdoux, or a child slave to Anakin to Vader, is always a question of ridding the conditions that forces one to choose or exist in such a manner. But to get where we want to go, we need the passage through CGI. JJ Abrams, claimed what was great about the original trilogy is that it felt real. That black and white good and evil distinctions have the air of the real whereas the impasses of democracy/dictatorship appears ‘fake’ should give us a signal to pause. The inability to believe CGI and political imagination is the true failure. As Dan Rubey pointed out in his classic article on _Star Wars_ for _Jump Cut_:

* Darth Vader’s use of the Force in the council meeting to control his opponent and Ben Kenobi’s use of the Force to get by the storm troopers;
* or the “bad” guys’ destruction of Alderaan and the “good” guys’ destruction of the Death Star;
* or the attack and penetration of Princess Leia’s ship by Darth Vader’s men firing laser guns, and Luke and Han breaking into the control room on the Death Star in the rescue of the princess;
* or Darth Vader breaking the neck of the technician on Princess Leia’s ship and Ben Kenobi dismembering the alien in the bar scene;
* or the pursuit of Princess Leia’s ship by the enormous ship of Tarkin and the pursuit of the imperial fighter by the Millennium Falcon; and so on.

…that is, there is no true difference. That the original _Star Wars_ could present these judgments in a register of realism, points to the dead end of realism as a register of thinking the present of choosing good or evil. In realism, there is no difference, and perhaps to see where the line between the two lines up, we need a different form.

![](/assets/uploads/Criticism_IM_Prequels23.png)

**5.**

Beginning with _Episode II: Attack of the Clones_ Lucas placed a primacy on the computer generated imagery, the use of blue and green screen, and the least amount of physical sets possible. On the first day of shooting, Ian McDiarmid would address crosses and markers while on a podium in an elevated pod. Brian Jay Jones wrote that many of the actors felt _anxiety_ when performing within the blue screen - crucial to note, as it is only anxiety and enthusiasm that do not lie. If anxiety, as relation to the real, appears when actors are surrounded by a blue screen, perhaps the filling of the blue can be called [_courage_](http://quacinema.tumblr.com/post/159034154769/kenotype-some-ideas-after-listening-to-franks). As Slavoj Žižek comments on Leslie Kaplan’s essay-poem _L’excès-usine,_ it’s not only that factory life is alienating as a self-enclosed universe, but the fact that this space is cut off has its own emancipatory actuality.

For what is the prison in _THX 1138?_ In Neil Bahadur’s reading, THX and SEN are placed in prison when logic and emotion are discovered. As punishment, they are in a white space without any orientation. In this space, conventional cutting and framing is _manipulating_. But perhaps we should instead see that the coherent representation of the space allows for determination and orientation, and it is _indeterminacy_ that is imprisoning. THX and SEN are unfree by fleeing from determination, the prisoners are all the more determined, unable to make a coherent space to track their own movements and exit. As any artist knows, it is the blank canvas that is ultimately imprisoning, and the first act of freedom is making a mark to orient oneself in it. Ultimately THX escapes and sees the sun. Bahadur claims that Lucas retrieves the world back from the deception of images. I would only add that by Monday, June 26, 2000, after seeing the sun, Lucas envelopes himself back in the prison, except this time it is all blue instead of all white.

![](/assets/uploads/Criticism_IM_Prequels24.png)

For the battle is to be fought here: there are no images, no world to reclaim that would be without _mediation._ What was a scene of disorientation in _THX 1138_ becomes the ground of orientation, of infinite possibilities, in the making of _Episode II._ The only escape from alienation is its redoubling.

**6.**

Where Lucas praises the freedoms of Soviet artists as opposed to his own freedoms as an American independent, it must be stressed that Lucas constructed a new alienation, that is, a new freedom: not a State art, but an art without the State, while never not confronting the state of the art.

After finally escaping the studio Lucas then faced the demands of the People, the mass audience, as an external measure of what he should do; this is one of the most bitter reversal of signs in the history of cinema, the reverse of the State and the Mass. His escape was a victory, but a bitter victory, as bitter as Darth Vader being born as Padmé dies. I was always struck by the beauty of the moment where Padmé “for reasons we can’t explain, is losing the desire to live,” - naming the children, she willingly dies, for Mother is not the destiny of woman, and children are not reason enough. The sheer heat in which these decisions are made is dizzying - before dying, with knowledge that Anakin has killed younglings, she _still_ tries to restart their love. It is this _stuckness_ to the real by the Skywalker family that forces history into _motion_. Which is to say no artist will ever escape external obstacles altogether but the problem is how to choose, how to choose our measurement and distance from the obstacle, which itself is a form of sticking to it.

What is worth sticking to, what is worth keeping? Even in a digital world, hair still moves in the wind, like Griffith’s wind in the trees, a minimal index that these actors are in the world that we see while also pointing elsewhere. Yet we stay here. In fact, it is staying in the present that is Anakin’s most difficult task, for he can see the future. At the close of _Revenge of the Sith_, we are also tempted to look into the future, since we know how it all ends, what happens in the other films made in the past but presenting a future, and we rush to make connections before they happen. We are, for once, within the subjectivity of Anakin. The temptation of those chosen is to know how things end and act with this knowledge. The bliss of the final iris to space is to know what it is like to be tempted by the truths of the future, and the future of truths – gladly acting according to what will have been, while changing those very coordinates in the same motion.

![](/assets/uploads/Criticism_IM_Prequels25.png)

![](/assets/uploads/Criticism_IM_Prequels26.png)

![](/assets/uploads/Criticism_IM_Prequels27.png)

![](/assets/uploads/Criticism_IM_Prequels28.png)

![](/assets/uploads/Criticism_IM_Prequels29.png)

![](/assets/uploads/Criticism_IM_Prequels30.png)

![](/assets/uploads/Criticism_IM_Prequels31.png)