---
layout: post
title: '"Carte Blanche: Towards A Tradition of Quantity” Q&A at VISIONS'
publish_date: 2023-08-06T20:35:00.000Z
author: Isiah Medina, Kelley Dong, and Isaac Goes
---
*The following Q&A took place as part of the program [Carte Blanche: Towards a Tradition of Quantity](https://lalumierecollective.org/2023/visions-isiah-medina/) (May 10 2023) at VISIONS in Montreal.*

**Benjamin Taylor:** Thanks for the program, Isiah.

**Isiah Medina:** Oh, thanks for coming. I had a ball, so.

\[Laughter]

**BT:**

To get the ball rolling… I was interested to know what else you are each individually sort of coming to terms with in these works. Because watching them, they don’t just feel like technical experiments, or trying to figure out how technology works. There are other things that you're thinking about. Maybe each of you could say a bit of a word. 

**IM:** Oh, for sure. Shall I just jump in?

First, yes, obviously there’s the technical aspect. But \[the name of the program], “Towards a Tradition of Quantity”, it's like – you know when Griffith made, I don’t know, 300 short films? Frampton makes a lot of films, Brakhage \[makes a lot of films] – there’s a quantity aspect. And obviously, it goes back to the Truffaut thing of being “against a tradition of quality”. It’s not enough to be against something; we should be able to name what we call our process. But I think there's a question of quantity where if you make enough films, you become fluent – not just technically – but just in what you're able to say. So I think it's also about that. It's not just a technical question. It's supposed to be like grammar, you kind of forget about it and start working with it. But it’s just to be able to speak in some sense. Griffith has to make 300 films before he does *The Birth of a Nation* (1915), right? So it's a question of quantity: How do you keep making films – make like, I don’t know, 20 films a year – so you become fluent with what you're trying to say, and be able to speak?

So, working with my comrades here on film – and it began with works by Alex \[Alexandre Galmard] – when I started working with him, our approach was “hey let’s drop like, three films this week. Let's just throw it on YouTube, and keep trying to find a way in to fluency.” And then when you become fluent, you stop thinking about it. In this case you start playing the T2i like an instrument.

And that's really important, because I feel like what's not interesting is a film that is not fluent with their medium. The more you make, the more you don't get concerned by this question of quality in the sense \[that] you get close to inventing questions of language. And of course, you know, there’s all these other ontological questions of infinity in relation to the notion of quantity, but I’ll just stop here for now.

**BT:** Kelley and Isaac, what about your thoughts?

**Kelley Dong:** What was your question?

**BT:**

So, when Isiah introduced the program, he mentioned this idea of coming to terms with technology. But \[within] the works, I'm feeling very subjective point-of-views, and each piece is maybe part of a process, in a sense, like it's a process in itself. I was interested in your own works, what you were maybe looking to get out of the process of making them, and also exhibiting, and what happens now?

**KD:**

I think we're all in our own way grappling – or we're each excavating our own archives. Filming with your phone, you’re now in a century where you are constantly archiving yourself, and constantly accessing your archive. For me personally, what I've come to uncover as my own interest is the speed at which you can change the shape of your archive, like the speed at which a memory passes through your mind differently than it was when you first lived it.

When I had lucid dreams as a child, I would stop the dream midway and change certain components of the dream. As I made more films I realized that is the pleasure that I derive from filmmaking. When I'm using footage from my phone, I actually am stacking footage from many years apart in the same frame. So for instance, the sky is from 2017, the color is a tint that I extracted from a train that I was on in 2019; I put all of those in one frame. The length of the film mirrors the speed at which the memory compresses itself and combines different memories together, and the memory passes by as if it was just 2 seconds. That is my process, personally.

**Isaac Goes:**

For me, at least for *Worlds* (2021), it's sort of like – I guess Isiah was talking about, you know, cranking out a large quantity of work. Whereas for me, I probably worked four or five years on this. So the quantity is all in this one picture. It's not, you know, separated into like, five pictures or something. I wasn't doing that much other than this work at the time.

For the most part, it is creating an archive of footage. I know it's sort of diaristic, but there are hypotheses or ideas I'm trying to use \[this archive] to convey, over time, what ideas I'm having – trying to express that with the footage that I have.

**Q1:**

I was curious, you were talking about \[making] a large quantity, and I was curious…where are you being the most creative? When you're capturing the footage? There seems to be a lot of cutting, is that where you're most creative, when you're editing the films? Or is it in the capturing? Maybe you could talk about the process between those – this capturing aspect, and then the editing. Which takes more time?

**IM:**

I think it's all the same. I feel like I shoot as quickly as I edit. I take the same time to do the budget, and it's all the same. You know, I think of it as all one big brushstroke. And to me, when I talk about fluency, the importance is how to get closer to the brushstroke.

So when I shoot something, and then when I edit – and I like to edit standing, so it all just feels like I'm still directing – I want it to all feel like one motion. So I wouldn’t say I spend more time with editing, or with shooting, or with budgeting. I try to make sure with those things, I spend just as much time, so it feels as easy as breathing. So it's not about like, “I put so much extra time in cutting.” I cut really fast because it's supposed to feel very easy.

**Q1:**

In traditional narrative cinema, sometimes the precognition of what that thing is, is done beforehand. Are you finding the film in the process of editing or do you know what you want, and then you're shooting it, and then you're editing it based on the initial \[predetermined idea]? Or are you discovering things accidentally in the process and liking them, then continuing in that direction?

**IG:**

I'd say it's a little bit of both. You have an idea of what you want and then what you actually end up getting sort of reshapes that idea. And it's a constant back-and-forth between the two, of this idealized picture of what you want even a segment of the film to be, and then actually trying to carry that out and put that into action. Then you realize okay, certain things are impossible, but here are the things that I now have that I didn't even think of. And it's that negotiation, that back-and-forth, that I think really shapes kind of everything. So it's a little bit of both, I guess, from my experience.

**KD:** I feel that if you plan well enough for what you shoot, you create an infrastructure for your editing process to be entirely full of surprises that still fit within that logic. I think that the misconception about making films that look diaristic is that we're just running out into the street with the camera attached and filming everything without discretion or discernment. This is not really true for me, and I don't think it's true for any of us. It has to be quite strict in the planning so that you're ready to confront yourself in the edit.

**Q﻿2:**

First of all, thank you very much for this program. The way you describe this tradition of quantity, I understood it as a poetic, and as a way of creating. But I feel like what we've witnessed in this program is also an aesthetic of quantity. And when I saw that…well, there was a Brakhage film within what I saw, so thank you very much for that. That’s a big flex also, to like, program your own films alongside a Brakhage…

\[Laughter]

**IM:** It’s all about flexing, you know?

**Q2:** Basically there's – I think in Brakhage, and in this program in general – there's this poetic, this practice of quantity, but there's also this aesthetic of accumulation, of images and sounds…

**IM:** Like between the process and the aesthetic, you mean?

**Q2:** Yeah basically. The aesthetic of quantity as such a thing…

**IM:** 

Obviously there is this question of density and speed, but it’s also about acclimating to it. I keep bringing him up because he is a classic, easy-to-understand example, but Griffith's intertitles last too long. I imagine maybe the people who are reading them are a bit slower at reading, but then as time goes on you notice the intertitles will get faster. Eisenstein was faster with his intertitles, let’s say. So they’re on this trajectory – not speed just for the sake of it, but just the fact of being fluent with cinema.

So I don't need that much time to see it. Serge Daney said that John Ford keeps a frame long enough for the eye to scan. Well, my eyes are really fast, so we gotta keep cutting.

\[Laughter]

And you have to keep it just as much so my eye can scan it; I know what it is, and we can move on. So it’s that type of thing. It’s how to get to a cinematic language that feels breezy, because it comes at you in a way that you're almost accustomed to, rather than trying to force a grammar that's not adequate to the thoughts we have about the world we live in.

**Q3:**

Hi, thank you for the program, I really enjoyed it. My question is for Kelley. I really loved watching your films, specifically your last one. That was *Two Dogs* (2022), right after the Brakhage film. I thought it was really beautiful and it made me think, or wonder about – because Brakhage was painting on the clear leader and sketching it out, and then you run it through the projector and it has this amazing frame-by-frame of that. But when you do it digitally, it's quite different, right? I'm just curious about your relationship to the labor of editing frame-by-frame, especially as you said, there's so much detail in one frame, images sourced from different places, and tinting, and it's just like a second, one-fourth of a second or whatever. So what does the work feel like? The labor?

**KD:**

*Two Dogs* took maybe a year and a half to make. And the one before that, *Pears* (2020) was one second, but it also took about that long. The process is just a lot of planning. For Two Dogs, for instance, I went out and watched and waited for the right dogs to show up, and I wanted them to go from right to left, so I needed to scope out the exact time and composition for them to do that. Usually I’ll film footage that is longer, then separate the frames and extract from there, and discard things in between so the footage doesn't have a sequential and smooth texture.

Within those separated frames, I start adding in layers from other separated and reattached footage. Then I'll stack footage on top of each other and I'll start color-correcting them one at a time, and the color palette will be a combination of the layers on top of each other.

In *Pears* (2020), the base layer of color is someone's naked back, and that is the orange of the film. One thing I find with digital is that it's really easy for your eye to think of it as a cheaper version of analog simply because it's not saturated enough and the shadows aren't dark enough. So most of that testing with color is just to find the right level of saturation and shadow that would make it feel as dense as watching a Brakhage, which has that density that just cuts through to you. But if I were to use phone footage, I do have to play around a bit with color. That's actually where most of the labor is, which I suppose is very Brakhage-like – I'm mostly just splashing colors together and seeing what the palette’s going to be from there. I don’t know if that answers your question.

**Q3:**

That's so cool. If I could follow up… To find the repetitive nature of working with such tiny frames or images, \[is it] like a meditation or something? Or does it feel like a lot…you said it was like a year-and-a-half process for those beautiful short films.

**KD:**

I find the editing really fun. It may take a year-and-a-half to plan, but then I edit it within like five days, 14 days…I finish it pretty briskly. 

**Q4:**

I’m new to all of your work, but I'm wondering – your group of friends, or people who know each other, working with similar equipment, and maybe some similar overlapping techniques, or not… In the 20th century, it would be an art movement and there would be a manifesto, and now people don't do that anymore. I wonder if you ever have conversations or thoughts around what it means that you're all working, where there's some overlap?

**IM:** In terms of like, theoretical discussions about what we're –

**Q4:** What it means that you're all kind of going in the same direction?

**IM:**

Yes, definitely. We really go into it and discuss it, and try to build a studio where we can continue to make them. We're definitely always discussing the nature of the cut. That was maybe one of the earlier theoretical points – okay, the cut is probably the material of cinema. It's neither the chemical – the photographic chemical stuff, nor is it digital. No matter what medium we have, it will be the cut, and the cut is an immaterial materiality…Yadda yadda. So – we start with the cut, or ontologies on the cut, so we start from the basis of nothing, because the cut does not appear. Then we keep building an ontology of why we're doing digital in this way and not another way, right?

So yeah, of course \[I go for] the cut, because there’s something specific about its relation in digital as opposed to film, and yet at the same time it's the same thing. You know, we constantly have these theoretical debates, and then when we’re on set shooting each other's pictures we're constantly theorizing while we shoot.

It’s funny, I notice so many MGM and Universal logos across some of these pictures – and it’s probably because we want to sustain the possibility of making pictures at the pace we do. So we don't put out manifestos and tell people. I feel like there is a secrecy that's interesting in terms of shorthand, and then working fast. Because once you have to start telling people all the time what you're doing, then you're explaining yourself about, “okay, so, ontology of the cut,” right…then I’m in school, constantly explaining, and once you’re explaining, you’re not making a movie anymore.

But once it's shorthand, then we can talk really fast about what we're doing, and then  we develop new techniques. So, there definitely is that level of theory going on. It's just not always public, but sometimes it is, in writing and stuff. To sustain ourselves as artists, we'd rather keep company secrets than have a manifesto. If there is a manifesto, it’s a company secret kept among working artists, it’s practical and not academic. 

**Q5:** Can you tell us a little bit about the studio?

**IM:** Like what it is?

**Q5:** Yes.

**IM:**

So we make movies and criticism, and we put it on this site called quantitycinema.com, but we also show the films. There's some theory on there, some of the films are there for free, and some are not.

Basically we all make our own films and we all try to help each other out on each other's sets in any way we can, anywhere from translating subtitles, to holding a boom, to doing CGI, to acting. We all try to take on \[all] the different roles. Kind of like that Marxist question of the polymorphous worker – you'll do some fishing here, some criticism there – that type of thing. We don't try to say, okay, this person directs, this person edits, and this person acts… We all try to do a bit of everything and we help each other on each other’s sets. And each time we do, I feel like we do try to take the next step from the last one; we never let each other repeat each other, in that sense. We can be pretty brutal with the criticism, I say…and then we continue on.

But yeah, just sort of the classic thing of trying to build a studio in that way. How are we going to make movies? How can we share equipment, or send equipment, send each other money, yadda yadda, so we can keep making pictures? Because it is experimental film, we do need that discipline and organization to continue to do it many times a year.

**BT:** What did you think about the Brakhage film?

**IM:** Oh, I had a great time.

**IG:** It was great.

**KD:** It’s great.

\[Laughter]

**IM:**

All I knew was that there was going to be clear leader, and I knew *Two Dogs* would follow with all the white. And I just thought, that will really work, even though I’ve never seen it. So I’m glad it worked out. And the colors remind me of *Worlds*, so it was actually perfectly matched against the cut. I was very pleased.

**Q6:**

I’m hoping there's a question – it’s a little half-baked – but when I was watching the films, I was thinking about the difference between exploring a medium and exploring the integration of the medium into life. And listening to you talk now, I feel like I'm hearing the recurrence of something about control: from lucid dreaming, to augmented reality, to practicing something to the point of fluency.

Is control comforting? Because I think in Brakhage’s era, sometimes I feel like, \[you’re] just bumping up against the medium and not knowing what it is. And I feel like your voices are about bringing a medium fully into and through your life and learning to handle it. I was wondering – this is the part where there's no question… Do you need it?

**IM:** Control?

**Q6:** Does it make good art, or is it a cause for panic?

**IM:**

I think with control, it's also about controlling your exact relationship to contingency. So it's not just to get rid of contingency, but how to control our relationship to contingency. For example, the Brakhage – I didn't even watch it, right? But I had enough of an inkling that it would work in that way. And now when I see it and all the phone stuff, when I watch it, it reminds me of scrolling because the images keep going from bottom-to-top.

So in the montage of the program, some of the Brakhage looks like scrolling to me, which was fun. I had no control over that; I had no idea it was going to make me feel that. But I feel like you do have to have enough control, so when the contingencies come about it's a nice surprise, rather than like, “Oh my goodness, what are you doing here? You didn’t control the variables enough.” It's not about just evil dictatorial control over all these different aspects, but controlling the spaces for contingencies so it can be delightfully surprising.

**IG:**

My movie *Worlds* is shot on 16mm, you know, in the 21st century where it's very expensive, it's not easy, so it's sort of walking this line between being diaristic and on the fly. But…we talk about every time you press the button, you kind of hear like a cash register. You know?

\[Laughter]

Just \[cash register noises] as you're going because, you know, you're losing money every couple of seconds you shoot, but you also want it to feel kind of free-flowing and like we're shooting on the fly. But also, I mean, you need to plan and ration your money. It's an interesting aspect of it.

**Q7:**

There almost seems to be – I don't know if I would describe it as a tension, but this feeling of like – this idea of the cinema of quantity, and then also the idea of this very finely tuned edit that comes about after the fact. Do you find there's a tension between the creative impulse that you want to carry through with this one big brushstroke versus this much more, like…how do you really reconcile that desire for impulse and freedom with this very finely tuned, very intricate edit that's going on?

**IM:**

I know what you mean. You're balancing the budget, number one, I would say. And then after that you balance it out in the edit. But I feel like that thing with impulse – you make a play structure like a kid, like a play space where you can be impulsive, but it's a safe play space. There’s places to fall. The rocks are little pebbles, so when you fall it doesn’t hurt like they’re sharp rocks.

It's why I feel like I almost take more time budgeting than I do shooting or editing, because once that clicks, then I'm just having fun. I’m just a little kid playing when I shoot and edit. But I need to have that structure so I can play. I don't want to play and then all of a sudden go, “oh no, I’ve got to make sure that's working because I didn't really think too hard about controlling that variable.” But if all the variables are ready to go, then I can have fun.

Then you could start improvising, inventing. But \[it’s] kind of like a jazz standard or something; it allows you to play off it and improvise, but you need to have something there. But that thing that’s there is just the plan, the budget, the shooting, the yadda, yadda…so you can be there and just have a good time and actually do it.

The only hard part is the planning, I think. The actual shooting, editing, everything is easy. It's just getting money in one place, kind of plotting it so you can do it. But once that's there, I really feel like it should be fun, because art’s supposed to be fun. I don't like the idea of like, you know, it’s this hard work, evil thing, and punishing yourself.

It should be a blast or else I don't want to do it.

**BT:**

\[I have] another question about maybe contingency, but in regards to what happens here in a cinema space and how these works work with each other, how they may work in other programs, how they work on repeat viewings for the same people, because I feel like all of those things will really change how they're received. And I could personally watch them a few times over and have a different but ongoing experience. This kind of falls into the idea of contingency – you're setting up certain circumstances, like we've done with you in space and you've done with the films, and we've got these things ready to go. But do you guys have interests in those different dynamics and how things can play out?

**IM:** In terms of projection?

**BT:**

Projection, I guess, maybe \[in terms of] your audience and maybe structuring a program… how they play out to people, how they reach people, and how many times.

**IG:**

Yeah, definitely. For instance *Worlds* is just on YouTube on a website I run called Kinet, which is a platform for experimental cinema. But I know we've always liked the idea of our films being on YouTube and not something like Vimeo or something like that, where it's existing amongst – you know, you can watch a philosophy lecture, and then our film, and then listen to music. It's just in the mix with a lot of other great works of art across a lot of other genres. And that way the algorithm can direct you to it, you know? Which I feel like has been helpful for me, at least, for people finding my picture.

**KD:**

My films are available online to download and because I personally like going through my own films frame by frame, I assume it would be the same for others. I think in terms of programming, we do like screening our films with each other and being the context for each other's works. \[This is] often stronger than a thematic structure in which films are extrapolated and placed with perhaps other –

**BT:** Films about dogs?

**KD:** Yeah.

\[Laughter]

**IM:**

For example, programming this carte blanche was like doing montage. I thought of it as if I’m editing a movie, and I saw these films side by side and scrubbed through them, like, “yeah, it looks like a nice brushstroke,” and I imagined this hidden Brakhage will fly.

And so, in the same way I feel about montage \[when] making a movie, you put the program out and see how it goes.

But I also \[included the Brakhage] because I haven’t personally seen it. To be honest, I do feel like it's important to personally enjoy what you're either programming or making. And if you feel like you're lying to yourself \[about] what you see, that's the first issue. First I have to at least know that I enjoy watching this.

**Q8:**

Do you purposely not put intertitles or credits in the program? Because then it felt like one long… like, I didn't know when one piece starts and ends you know.

**IM:** 

Well, Isaac's had titles. The other artists decided not to and I wasn't going to force them. And Brakhage had a title. There are program notes, but it's also nice to be lost in the films. I guess that's part of the possibility of, “Oh, did this film start yet? What am I watching? And what is this tradition of quantity?”