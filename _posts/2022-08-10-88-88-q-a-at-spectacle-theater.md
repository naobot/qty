---
layout: post
title: '"88:88" Q&A at Spectacle Theater'
publish_date: 2022-08-10 09:58:01 +0000
author: Isiah Medina, Myles Taylor, and Steve Macfarlane
---
*The following Q&A took place as part of the retrospective* *[Isiah Medina: Films 2010-2020](https://www.spectacletheater.com/isiah-medina)* *(July 29 to August 21 2022) at Spectacle Theater in Brooklyn, NY.*

**Isiah Medina:** This film was made 7 years ago, it was my first feature. The title *88:88* comes from when you can’t pay your bills and your heat, water, and light all get cut. When you’re able to pay again, all the clocks and digital appliances flash 88:88 as both suspension and reset. When I first showed it years ago at its premiere, I was nervous, and then I showed it more and more and I wasn’t. And now years have passed and I’m nervous to see it again. It’s like another reset. I feel nervous because I feel like I am not only sharing it with you for the first time, but I’m even sharing it with myself.

\[Screening]

**Steve Macfarlane:** Well, I'm remembering when you were here seven years ago for your Without A Future event, we thought about having a secret bootleg screening of this film here. But it was too hot, and we reconsidered because of the New York Film Festival premiere.

**IM:** Oh really, I remember we played it here right after actually.

**SM:** I know.

\[Laughter]

**SM:** Print the legend. I still find *88:88* totally radical and incendiary in its editing strategy. There’s all kinds of things I want to ask you about. But starting from the very end, \[there's] something I was too scared to ask back then, which is where does this sound effect come from \[makes noise]?

**IM:** I was collaborating with an artist, the musician Kieran Daly. I’d ask him to send me sounds that have that descending quality. He’d make or show me sounds, or I’d send him sounds and then he’d play with it, and he’d send it back for me to play with.

**SM:** So you don’t know if it’s captured and then manipulated versus it being a synth, or completely created in a studio?

**IM:** I’m not sure, I don’t remember actually. I just asked for something that sounded like it was descending or powering down and he’d send me different interesting options that I would shorten or stretch or layer or use as is.

**SM:** Incredible. Could you talk a little bit about how what they call wild recorded sound influenced your editing strategy, not so much editing in terms of montage, but just constructing and designing the story or the recording? So much of the film seems to be structured images accompanying, you know, long audio tracks of what sound like interviews or conversations you’re having. How does listening to that long-form audio influence the way you construct the quote-unquote "story," since I know it’s not a story…

**IM:** You just find that listening is important. I’m willing to sit down and listen to a lot, and learn how to filter what’s most important to listen to. Grothendieck compared mathematics to listening. I thought about that a lot while I would record, and also to be ready to pull out my phone and record if I had to. But also since the film was made with my friends and I had a camera since I was 12, they’d be used to me recording them speak anyway, since we’ve been doing this for some time.

**SM:** Was it audio only?

**IM:** Sometimes it would be shot as video and I’d subtract the sound. Even with a picture that’s playing tomorrow, *Semi-auto colours* (2010), a lot of the sound was recorded using my video camera while I would shoot with the images with a Bolex. So if the image and sound are fused and synced I would divide them, and learn how to think with them by themselves through this separation.

**SM:** I guess I consider it more sound-centric or sound-focused than a lot of quote-unquote “avant-garde” cinema I’ve seen in the last 5 or 10 years, like you’re letting the sound dictate the editing strategy, or the narrative as it were.

**IM:** There was a point when \[Richard] Diebenkorn complained that with abstraction at the time “there was nothing hard to come up against," and figuration is something hard for abstraction to come up against. I find that in cinema, with montage, if you don’t have enough sound ideas, you have nothing to hit against. Sound is almost more figural than the image, at least in a film because of the force of language, so I want to construct something in sound that the image can hit against. The sound must have a definite shape. And then I can abstract around and with and through it, because then we are dealing with language, something hard to come up against. I think the big dialectic in cinema is between montage and writing. And writing is another name for a sound that is different than what is written.

**SM:** Did you coach people to talk about things that you talked about with them so you can get it on your microphone?

**IM:** Sometimes a friend would tell me a story and I’d ask to be told that one again. Or we’d be in the moment, I’d pull out my phone and Myles would recognize I’m recording and continue. Or I’d have a bunch of poems in my pocket and I’d ask “can you read this one"? Or if another friend has a poem, I’d be like, “Let’s hear it." Or I’d write other things or read something from a book. But watching it again, I really feel the material fight to read anything at all. When I see the economic reality we’re living in, to really fight to think philosophy and read these things. And having that gap, or perhaps not a gap, between what is being said and what we see, I think again it’s that fight between writing and montage.

**SM:** So you start recording, and they pick up on that. It’s not too different from quote-unquote “mainstream” documentaries are made. But so much of your film I felt like was setting up these things and then disrupting them, or showing how easy it is to disrupt them and show how constructed they are. So I guess this is a joint question for you and Myles: the sequences where Myles’ audio is incorporated into the narrative, how did you first see it, Myles? Was it played to you after being cut, or did you workshop it through multiple different iterations?

**Myles Taylor:** No, no. We’re friends, so we used to really hang out and share life experiences while we were going through them. To be honest, a lot of my friends are musicians, filmmakers, poets, rappers, artists, and it’s just a melting pot of all that. I used to write rhymes and stuff, and a lot of that was very amateur. Isiah picked out things to include from what we were living. Like you said, smash against the image. It was just our lives at the time. A bunch of young gentlemen from the West side of Winnipeg and all our friends. I’d have rhymes and poems on my phone, and another gentlemen, Erik Berg—who's a poet and actor—was also well-versed in putting out things. Isiah would tell me certain things, or point to rhymes I would write or passages from a book, or the experience I was having.

A lot of it was very impromptu. Like if I messed up \[Isiah] would just keep it. We were very young—to be honest, I’m watching it at 30 years old and I’m like, “Wow, that was brilliant.” It’s very wild and the environment we were in was wild as well. Just putting together the things we would do, but he’d throw it together in such a savant way. Even to this day watching it, there’s certain shots in it, I’m just like “wow." It’s just a marvellous piece of time and art from an area of my city that really didn’t have much to offer. We made something out of nothing and now we’re here at Spectacle, 7 years later. I just think it speaks to young people putting what’s in their mind out to the world and putting what’s happening in their lives to the world. It’s a lot more accessible now with social media but 2009, 2010 was the beginning of that.

**SM:** For me, it’s just as incredible as when I first saw it at TIFF 2015. Some parts of it appear so raw, rough, and wild, and then other parts are so heavily constructed. The second thing makes me go back and consider the first thing. I’m always wondering, was half the production walking around and recording everything and editing it?

**IM:** No, I’d know exactly what’s going on. If I’m shooting that day I know exactly what we’re doing. Even if I don’t tell everyone. But I would lead people into situations. Or for example I’d talk with one of the cinematographers, Nic Kriellaars, about brain sciences and German idealism, diagonals in Lumière and then lead him to a location and say okay, now try to find what we talked about. And he would find it. But you position it in a way that these things can be found. Same with sound. It’s always a choice. I’d never record *everything*. It’s always very intentional. Honestly, there’s a simplicity to it. If I knew we were shooting that day—and we didn’t have call sheets like we do now—I just know that if enough of my friends weren’t working or didn’t have a shift that day, I knew if we met up we can make cinema for sure.

**SM:** And were you determined to show, in some way, that you could make a full film for $0? Or was that more like an outgrowth of organic processes developed for X number of years?

**IM:** Obviously if there’s no money, I’ll use no money to make it. If I show up and we have a camera and tools to edit, I’m making cinema, period. Whether it’s $100K or $0 or -$100K. You just have to have the confidence to make art, or why are we showing up? You have to show up and think without a doubt we can do this.

**SM:** This is still a very confrontational mode of montage. So I’m wondering what did this film do for you, what was your festival experience? Did people expect you to turn in something more quote-unquote “mainstream” after this? And it didn’t happen or…

**IM:** *Inventing the Future* was mainstream, right?

\[Laughter]

I have no idea about anything regarding festivals. I just made movies. I was very naive. I had no idea, I just went to these places and I thought there might be some payment.

**SM:** And that didn’t happen.

**IM:** Here and there. But mostly it’d be festival to festival, traveling, but no payment at all. There might be a hotel or a flight, sometimes a meal. I’m not going to lie, I naively thought if I made a masterpiece then we’d have a bit more money in our lives, but the money didn’t come. But... I think the movie is quite good.

\[Laughter]

**SM:** You’re also still in your life, you can get money for this masterpiece later.

**IM:** It took time but I have my own equipment now, so I can keep making pictures. So it’s all good. But whether it’s an iPhone or a RED or whatever you can always make a picture. In terms of festivals, I just liked seeing the movie on the big screen. I didn’t get paid but I enjoyed seeing it on a big screen. I often can’t afford to rent a theatre. What I get most out of the festival experience is seeing my work on a big screen. Seeing my movie is really nice.

**SM:** This film, like *Inventing the Future* is online in full. And some of the shorts are online. Has that gotten you anything specific, where something incredible happens that can only happen because you put it online? Or do you still prefer it in a theatre?

**IM:** Obviously, I’m an old fashioned filmmaker. I like the big screen and everything, it’s the medium I chose. But when I get a message from someone I don’t know who liked the films, I find it nice that someone I don’t know saw it all on YouTube.

**SM:** And that happened with *88:88*?

**IM:** With all of them, but obviously it’d be nice if they could see it on a big screen. And these days I’m more sensitive over fighting for the right to appear on a big screen. But say if someone messaged me liking my previous work, I’d slide them the link of the newer one. I thought it was a generational thing to do so. I’m less sure now. In the end as long as people see it, but I do make films for the big screen in the end. You can watch it on your phone, but please don’t.

\[Laughter]

I know people have, it’s totally fine, too.

**SM:** You’re envisioning scenarios where no one comes to the screening because they’ve all seen it on their phones. Do we have questions from the audience?

**Q1:** What’s your editing process like, is it really intuitive? Are you just trying things out? How do you know when you’re done?

**IM:** I feel like I would have a much more theoretical answer if I was asked this some years ago but now I know that I do intuit my theories. So intuition is involved, after I’ve theorized. The more you theorize, the more intuitive you can be because your intuitions will have practiced following rational links to get from one thought to another, via negation or other methods. For this picture I was thinking of philosophy, and I was thinking of mathematics to train my intuitions. I’d think of the movie set in relation to set theory. So I’d have a shadow of someone with their hands up looking like the omega symbol, because that’s the first infinity. And infinity is a cut and recollection of the form of counting, so I would link this to the cut of cinema. Or we can look at the handcuffs when open also look like an omega symbol. So it’s these types of games I like to play.

You can play it on the timeline and you’ll know if it gives you that funny feeling or not, and you’ll know whether to move on or not, by the fact you can both intuit the theory and be pleasured by watching it. I also edit while I shoot, so I have some of the edit complete while I shoot new sequences, and I’ll know how it will fit. The previous edits become a filter of the new shot, and vice versa. A simple example has to do with shapes. There’s the shape of the omega, the shape at the level of its appearance as an object in frame and the shape of the timeline itself that is structured through these ideas on infinity. So you have shapes on your timeline and then you go out into the world of your set and try to find those shapes again, or a different shape. But in the end you yourself should enjoy the picture, it’s for your gaze alone.

**Q2:** In discussions of documentaries, people often become obsessed with the effect the camera has on the people being documented. But I’m more curious about the effect it had on you. In terms of bringing the camera and the sound recorder into your interactions with your close friends and documenting so much of what was going on in your actions with them, how did that transform your relation to the kind of experience of being in that context? And how did it transform it? How did the camera affect you?

**IM:** Yes it affects me, perhaps the way a paintbrush might effect a painter, it just becomes a part of me, that I don’t count myself as myself without some of these tools that make my mind what it is. But not because I’m always shooting things, always whipping out my camera. When I experience what I experience, it’s not like it’s in cinematic terms. When we experience anything we’re in an infinite set, and there might be an axiom of choice where I can choose one thing over everything else. So the effect of the camera and the editing tools is that I learn how to be alert to choosing among infinite possibilities. So I feel like its effect is that it sharpens my senses even without a camera.

You know the story of Brakhage going to the doctor and getting his eyes checked and the doctor sees that Brakhage’s eyes are faster than his other patients? Making movies has this type effect. I can <covers eyes with one hand and air types with another> use Final Cut in this way because you’re trying to make it more and more natural, so making film is like breathing. So I would say it doesn’t have a negative effect on me, or make me self-conscious in a pejorative sense. When people act differently in front of a camera or if my senses get sharpened because of my relationship to filmmaking, it just means reality is incomplete—someone acts differently because of these tools shows me there wasn’t a stable reality to break from, but we are in the act of creating it.

Or even with this picture, me being in the film had more to do with friends having shifts. I just had to step in because we didn’t have enough actors, you had to have this ease of creation. Making films teaches me to listen better, and not just when I’m recording. It’s about how you listen to people, and really hear what they’re trying to tell you so you can think with them, it’s really that simple. Making movies made me into a better thinker.

**Q3:** The movie is so beautiful. I was curious, I haven’t seen *Inventing the Future* yet, but is there a relationship between this film and the other films you made afterwards?

**IM:** With *Inventing the Future,* I wanted to take a step forward. I didn’t want to get caught in re-enacting the same struggles with better lighting, because that’s not interesting to me. You can catch lightning in a bottle several times in a row but you should also know when to move on. I thought if the people in the film—as people or as characters—are dealing with their impoverishment then we should think about UBI, changing the work week, a different world, post-capitalism. If you really care about who is on screen then you do want to think of the future, you don’t want to see how sad the situation can possibly get and get a well photographed and well performed picture of that, because I don’t find it dignifying. So you want to think about philosophy, and then later you want to think about politics.

You want to think how to zigzag from film to film, to self-consciously contradict oneself, to grow as an artist and thinker. *88:88* is clearly about the present, how the present presents itself in the suspension of time, and *Inventing the Future* thinks the future. But also if we dealt with non-sync sound, then later you want to integrate more sync sound and find what new relationships between the sync and non-sync... Since there was some use of the tripod in *88:88* but it was mostly handheld, in *Inventing the Future* I wanted to put the camera on tripods the whole time, because the film is also about being a bit "hands-off" so your hands can work on other things. Or we work with a screenplay a bit more another time around, and keep shifting the method, not out of eclecticism, but in the zigzag you will find a stronger unity in these opposites. If one gets too comfortable it can be boring, and you’re no longer growing.

**Q4:** In the introduction to this film, you talked about the title of the film to a reset space or a transition space. And I was wondering if you can talk a little bit more about how you’re thinking about this transition space throughout the course of the film.

**IM:** I think *88:88* is less about transition, and perhaps *Inventing the Future* is the film where I’m trying to think about transition, the transition of one society to the next. With *88:88* I was thinking more about the abruptness, the stop-and-go of being poor. It’d be nice if being poor had smoother transitions but it’s a bit more like one abrupt thing after another that’s hard to deal with. So it’s more a question of this reset, being stranded at a beginning, the inability to move forward or to get to the luxuries of continuity editing, because one’s life does not something that exists in that space.

It’s this question of how to read that abrupt "88:88," a reset that resets nothing. How to experience it as affirmative and not only a negation, or one type of negation, how to look at the open handcuffs as an omega, how to get back to an infinite way of looking at what you’re going through, how to look at those 8s on the clock sidewise to see all this other stuff. But it’s also like, rather than force a pattern on reality, how do you, to return to your question, being in cinema, look at these 8s that keep appearing in my house in a different way, that’s not just one form of negation, and how to negate that, et cetera.

How do you find patterns in reality, and once you recognize it, pull it out, and use it as a filter to see the rest? In that sense it is about transition, how to pick out this unique thing, and use it as a filter to cognize your reality. And at the same time know it’s not the only filter, but it’s a locally useful one to cognize what you’re going through. It’s a transition of the mind first, during this reset, how can you transition in regards to your mind to face reality differently.

**SM:** I was always incredibly impressed with how beautiful the film was, as you heard in those comments, but also clearly not. With these crazy tracking shots or drone shots, \[and] even the colour correction added in post is clearly artificial, it’s added to the image. So there’s something about stopping and taking in these poetic, beautiful moments, but also not romanticizing poverty. There’s the pattern-seeking that you’re talking about.

**IM:** Yeah, like you’re saying, we shouldn’t aestheticize poverty, and we shouldn’t aestheticize work, which is the other one.

**SM:** The two genders.

\[Laughter]

**IM:** There’s many contemporary filmmakers who want to tell you how much hard work goes into making pictures and I just don’t want to romanticize that either. And when it comes to beauty, it’s like with Plato: the most beautiful thing is the idea of beauty itself, its concept. So it’s already desubstantialized, beauty is beautiful. And keeping it conceptual allows you not to fall into these traps of making something that looks like a commercial.

**SM:** Did you ever think you were close to that?

\[Laughter]

**IM:** If I say there’s a beautiful rose and the rose is beautiful, the second one is more interesting because the rose participates in the idea of beauty. If you attach beauty to the object beforehand, and the thing itself starts shifting and changing, you may be on a wild ride to maintain an old standard, as if the standard belongs to the object, which can be reactionary.

**SM:** So keeping an established beautiful thing beautiful is reactionary whereas showing something as it is and letting people apply their own sense of beauty to it…

**IM:** It’s important not to get caught in the appearance of beauty, which is different from the being of beauty. The appearance of beauty can get us into a sort of market view. When you see a really bad kitsch painting, let’s say of angels floating around and one painter may do it and it’s sublime and someone else may do it and you’ll think “Oh please don’t do that” because it’s awful. It appears to be the same subject but it’s not.

The idea of "angels floating" is not in itself beautiful, one must artistically find a way for it to participate in the idea of beauty. The bad painter may think angels floating is itself beautiful and attaches the beauty to the object, substantializing it again, rendering it awful.

**SM:** I remember in 2015 we were talking about the sequence with the handcuffs and you shot it on the RED with a wide lens and how it’s intended to be quote-unquote “cinematic.” In other words, it looks like something from possibly a fiction film and it’s jarring when it arrives in this film, because so many of the other images have this handmade quotidian quality.

**IM:** A lot of the other scenes use the same lens and the RED, and there’s still the so-called handmade quality in not cutting out the adjustment of the camera, another suspension, here the suspension of the gimbal snapping into place. In the end it’s more the fact that when you have this tracking movement of the camera you must have a reason. Here when you see the prices moving on the ticker in the Financial District in this horizontal fashion, the tracking of the camera is linked to this movement that is seen in the frame. So we’re back to the concept of the very way we shoot is immanent to the image itself, the part of the image we chose.

Again I chose something that was in the world, the movement of the prices, and matched it, in order to think it with cinema, and then we can think it in the cut. So it comes from choosing something from the possible frame, pulling it out, and using it as a guide for what’s in the construction of the frame, so the form remains immanent rather than transcendent, from the outside.

**SM:** And it works within the montage that you’ve been establishing, it takes you out of the movie but only so much, and then you’re back into the movie even more perhaps.

**IM:** I think constantly being pulled into the movie and pushed out is its own suture.

**Q5:** Based off this previous question, there’s a kind of resolute refusal to sync sound to image. And I would love to hear you say more about the significance of that. Because think about how easy it is when we’re watching a film of 30s cartoons showing us that we have a natural tendency to sync sound and image, if you hear any sound at all, then there’s a movement on screen that corresponds to that. We think of them as unified, presenting a single action. And you have, as the conversation brought out and as we saw brought out, refused that kind of sensory integration. And so I’d love to just hear more about the significance of that for you, what should we be appreciating about the film because of that?

**IM:** I remember there being a couple times where there is sync sound as we understand it. Some of the screaming is synced, and there’s also a scene when Myles talks about something real Roger told him. But also in terms of sync, there was a time when people thought the sun went around the earth, and there’s a sense that theory was synced to our senses. And then there’s a new theory and we realize our "senses" were wrong. And here, I think with some adequate theory we can say that this classical sync is not the only correct way of doing things, the only way to present the category of reality.

When I’m thinking about something while walking down the street, is that synced or unsynced? Are my thoughts that I think I hear, are they synced to the world around me, to this and that, or is it unsynced by the nature of me thinking about it? Say I have a mathematics problem that is really troubling me and I’m walking around, but since I’m dealing with the world, written in mathematical language, I’m probably unconsciously dealing with mathematics all the time. The unsync and sync question I feel is undecidable, so you have to make a choice. If you have a new idea, it’s synced for you but may be unsynced for other people. And then you start the slow process of inventing a clapper that can sync the thoughts to the world, so others can understand the sync I am aiming at.

In another sense the movie is perfectly synced, like all the relations of the frames and the cuts and the sounds do correspond to a single movement. So it becomes a question of what counts as synced, or even what counts as a single movement or a single action that the various elements are synced to. So there’s nothing natural about sync in terms of editing, and again there’s another scene when someone says they are hearing voices, and what’s that relation to sync and unsynced sound? So it’s about taking very seriously why we sync, what is synced, and when, and to think about it very concretely.

**Phil Coldiron:** There’s also a quick funny joke with Avery walking out, right? With the synced sound, and then a piece of dialogue that leads into that, saying something about preferring simple things.

**IM:** There’s a sense that a unified artwork, in the last instance, is synced with itself, syncing the sync and the unsynced.

**SM:** I was confronted again, with this whole thing of how it’s much more intimate to listen to an audio track without a corresponding image and to try and suss it out and figure out what the fuck is happening. Doing that work, which is usually relegated to radio or some sort of surveillance, forensic type listening, in a film is still very forward thinking.

**IM:** It’s not given. If it’s not given, you have to make a choice.

**SM:** Sync sound is the most taken for granted you might say. If you’re going to watch any audiovisual thing, at least the sound is going to line up with the image, that’s a bare minimum expectation. Do we have any other questions?

**MT:** I have an answer to my own question.

\[Laughter]

\[Isiah] didn't ever just record whatever we were doing. He has always had a very impromptu fashion, as well as a very mildly meticulous and very wildly meticulous way of doing things. And it shows now when I watch this film again, because he filmed a lot of this at different stages, and then finished the film in Toronto. So some of this stuff was filmed over probably a year and a half. The way he did everything up until the end of the film is just exactly what you said: while he was doing it, inside the camera affecting him and other people, he becomes a better thinker with different things that are coming in, the images and sounds. Even now, after all the time I've known him, and the films he's made since this film, it's very distinct. I feel like if I didn't know him, I would be very in awe. I think it changed Canadian cinema to be honest. It's like one of the most perfect films I've seen. If I wasn't in it... There's certain things about the film that I relate to, certain things that are not me in the film. It goes back to Alain Badiou's love-science-art-politics, and with everything else in life, just throws this curveball. He just, Barry Bonds that shit.

\[Laughter]

He hit it way the fuck out of the Giants stadium. Seeing it now, even the second half of the film is performed so well. The images and sound sync perfectly in my view of everything, because imagine when you go to scream, and then your body stops the scream. Like you're trying to do something.

**IM:** Yeah, exactly.

**MT:** And yeah, I haven't seen it in a long time. It's a very beautiful film.

**SM:** How many of y'all had seen it before? Nice.

**Q6:** \[To Myles Taylor] How did seeing it on the screen, transform your own experience of the things that you experienced in the first instance?

**MT:** A lot of things tend to relate to now. We don't necessarily change as people. You may change your being, you may change your lifestyle, change your body, all these different things, which I don't necessarily think we change. I think experience and the notions of experience and the things you go through change you. But I still see myself very much as that... Maybe I've matured, but \[the film] kind of related me back to the point of what he was saying, the nuance of getting out of poverty and becoming a different person. And just like you said, contradicting what you were already doing. And that's my life now. I'm not the same person, but I am still that person.

It changed the way I look at myself. You see a younger version of yourself and the things you've gone through since then... It's really weird, because some of it is me saying something and like rehearsing something, which is my own, or a poem, or whatever it may be. But most of it is just really our lives. I don't know if I'm diverting from your question a little bit, but it makes me look at life a lot more intuitively. You knew everything was gonna work out, because you knew you're the person that you are.

I don't even know how to answer that question because we're all growing until the day we die. And seeing people and filmmakers make film is like... You know only the best is yet to come with life and with art, with creation, and the negation and addition of other things. It's a long journey. This film shows part of my journey as well as his and others in the films' journey in life. I chilled with a couple of actors in this film last weekend in Toronto, so it's the dopest feeling.

\[Laughter]