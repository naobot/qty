---
layout: post
publish_date: 2017-06-19T17:39:25.000+00:00
title: On "Demolition of a Wall"
author: Isiah Medina

---
![](/assets/uploads/Criticism_IMDemolition1.png)

![](/assets/uploads/Criticism_IMDemolition2.png)

![](/assets/uploads/Criticism_IMDemolition3.png)

**1. THE REVERSE**

a.

In 1896, Louis Lumière produced an actuality named [_Demolition of a Wall_](https://www.youtube.com/watch?v=9p0HI9t5IB0)_._ The movie presents actuality as revisable: it includes a reversal of motion.

b.

Does this film contain one or two shots? One can count the entirety of the film as one shot, or one can claim that at the point of the reverse a second shot begins.

What we traditionally call a shot-reverse-shot is the same as a shot-counter-shot: one actor looks at another, who may be off screen, and after a cut, one sees the other character looking back at the first. Actors face opposite directions and eyes are aligned, so one gets the impression theyʼre looking at each another. This technique is often used in dialogue sequences and is a hallmark of “invisible” editing in the classical continuity tradition.

If we count _Demolition of a Wall_ as a shot-reverse-shot/shot-counter-shot because the two shots unfold in opposite directions, we can say that the image is in dialogue with itself. In film terminology we have two terms for the same technique. For the purposes of this essay I reserve “shot-reverse-shot” to refer to _Demolition of a Wall_ as one shot (there is a shot and a reverse, which is a continuation of the same shot), and “shot-counter-shot” to refer to _Demolition of a Wall_ as two shots (we count the reverse as a cut).

c.

All the frames in the movie appear twice. When considered in the order of their appearing, one particular frame is repeated beside itself. This is the moment of the reverse. These two frames produce a freeze, however brief. When movement moves in an opposite direction, it also produces what is opposite of movement itself: stasis.

The brief stasis appears in the midst of consistent motion: the appearance of the single frame is bracketed by two identical frames. Within the intermittent, illusory motion, an opposing stasis also appears. This binary stems from an internal split: the same image appears in two different frames identical in content, but in minimally different places on the film strip. This minimal difference of the same frame, twice placed, forces the frame to appear _as_ a frame.

The difference is crucial: we cannot conflate the frame with reality itself. One cannot conflate sensing with thinking nor watching a frame with watching _as_ a frame.

d.

Watching _Demolition of a Wall_ as shot-counter-shot, the first shot appears natural and the second shot conceptual in its point-by-point reversed repetition of the demolition.

As shot-reverse-shot, _Demolition of a Wall_ has the initial natural appearance pointing to itself in the reverse, and in this self-referential activity navigates away from what appears as naturally given towards the conceptual. What appeared as a natural image in the world of physical laws becomes a semiotic, historical image. Our immediate perception of causes passes into a mediated sign of reasons constituted by the same frames. The distinction between the natural image in the manifest direction and the historical, reversed, conceptual motion is a matter of organizing the frames. The dualism between natural and historical appearing is formal, rather than substantial.

An editor who runs their rushes back and forth sees this movement often. The frames of what _is_ can be transformed to what _ought_ to be. An editor can give reasons as to why to cut into the order of manifest appearing, and propose a different order. The rationality of this order can be judged by anyone who watches. By witnessing a reorganization of our natural frame, a viewer can reason whether the connections and disconnections between single frames take place within rules that one can follow, or not.

What is implied in our linguistic considerations when watching a movie can be further explicated by allowing those thoughts to follow the patterns of framing and cutting that ensue.

The shot-counter-shot between nature and history also takes place between the movie and the viewer, where either can be speculatively reversed as determining the other. Where the switch of the cut takes place, and whether the cut is by the editor that separates or the intermittent mechanism that holds together, cannot be known in advance and is an object of careful study of the concrete movie.

**2. THE REVERSE OF THE REVERSE**

a.

Related to shot-counter-shot is the [Kuleshov effect](https://www.youtube.com/watch?v=_gGl3LJ7vHc), which preceded it. In 1921, Lev Kuleshov completed an experiment in which a shot of an expressionless face alternates with other images, including a plate of soup, a girl in a coffin, and a woman on a divan. Audiences project different emotions dependent on what shot follows (or precedes) the face, which remains constant. In principle, the shot-counter-shot and the Kuleshov effect are the same, as they rely on eye-line matches, and implicitly the 180-degree rule (though not necessarily at a close or face-to-face distance; the effect works for any distance between subject and object). One can imagine a face talking and a cut back to an expressionless face and we can infer that the face is listening, or reacting, especially if we hear dialogue as well. The audience often projects its subjectivity into the visible human figure, especially the face.

However, why must this logic of dependence, entwinement, and the back and forth find its anchor only in the expressionless human face? The face can also be smiling in each shot and yield a similar differentiation in reception. What is crucial is not that the face is expressionless but that it is the same. And if what matters is that the frames are the same rather than expressionless, do we need a human face to inscribe this pattern of thinking?

b.

At the moment of reversal, I marked out that the same frame was repeated side by side in order to change direction. If we see _Demolition of a Wall_ as a shot-counter-shot, the cut would place itself in this stasis. If we see _Demolition of a Wall_ as a Kuleshov effect, we can place a viewerʼs projection of subjectivity not onto a consistently expressionless face but instead onto the cut itself: the cut appears the same in every iteration and expresses nothing. The de-naturalization of appearances is found not only in the visibility of reverse motion, but in the dis-identification of subjectivity with the human figure in the frame, moving towards an identification with the cut.

The subject, determined by the spiral of nature and history, is not given, but must be found in the trajectory of the reversals. If the cut is the appearance of negation, and an editor decides where to place the cut by playing rushes backwards and forwards, frame by frame, then one of the origins of cinematic negation can be found in _Demolition of a Wall._ This is to say: even if one reverses the reverse, and an editor makes no cut, we are not simply back to a “normal” motion. Classical logic does not hold. The cut carries the phantom of the reverse in all its incisions, even when no incision is made.

c.

The appearance of the reverse motion does not mirror natural laws, but its motion is embedded in real physical patterns. If we count the moment of the reverse as a cut, then the cut can count as an instance where a concept is sensed. Similar to the positing of “invisible” theoretical notions and entities to explain our phenomenal perception in the natural sciences, the cut is a governing principle that grounds what is visible to us in cinema. In this sense, the “invisibility” of the shot-counter-shot within classical continuity editing can be taken up as a challenge to not only make visible what was once invisible, but to produce a new “invisible” edit: a non-classical continuity editing that invents a new manifest image of the world.

The recognition of the similarities between shot-reverse-shot, the Kuleshov effect, the parallel edit, and the match cut, as they link to the idea of the cut, is to understand that these concepts link to one another and form a material pattern anchored in natural processes.

To continue this investigation, we can see _Demolition of a Wall_ through additional techniques (seeing the movie as parallel editing, as match cutting, etc.), expanding our notion of cinematic subjectivity as an event in the cut. The cut need not only apply to a frame rate of twenty-four frames per second in forward or reverse motion. For instance, in the way reverse motion produces stasis, _acceleration_ of the frame rate produces slow motion, rather than speed. Acceleration of the frame rate allows for the analysis and pinpointing of various pivots in movement, slowing down the appearance of the world in order to study how movements occur and how it can be redirected, repurposed, and re-cut. The gesture of relating to a human figure in frame to the cut itself in our renewed understanding of the Kuleshov effect repeats in a shift from the primacy of the “human” frame rate of twenty-four frames per second towards a primacy of accelerated rates.

d.

By combining variations of cutting and frame organization, we can rediscover our history and find our framed, natural, manifest image transformed. Cinema can present our sense experience as conceptually tractable in the rational subtraction of the cut. _Demolition of a Wall_ will be subject to revision as we continually revise the nature of our subjectivity in the unfolding of cinemaʼs history.