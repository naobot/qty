---
layout: post
publish_date: 2022-09-02 03:41:55 +0000
title: From Paint Tube to YouTube
author: Isiah Medina

---
_The following essay was published to accompany the retrospective_ [_Isiah Medina: Films 2010-2020_](https://www.spectacletheater.com/isiah-medina) _(July 29 to August 21) at Spectacle Theater in Brooklyn, NY._

Distribution is not separate, but part of the form of film. And like any part of the form, distribution must be controlled, the same way the artist controls the light, the cutting, the writing, the performances, the camera movements, and the like.

For Pierre-Auguste Renoir, without paint tubes there would be no Impressionism. We can talk about paint tubes and not having to grind your own colours, not having to be an apprentice to a master. We can also talk about YouTube which means free tutorials, immanently large parts of the canon viewable for free, unlisted links of finished or unfinished films to share—and to leak.

But for things to be truly infinite, they must come to an end. Without the figure of the end, we are within the finitude of an opening. Things are infinite because they close.  Freedom is to open and close at will.

This retrospective allows me to close part of my project as it exists today. For at least ten years I’ve premiered or distributed my work online for free. Starting September 1st 2022, my early work will no longer be continuously free to stream on YouTube, Vimeo, or anywhere else. With the exception being _Inventing the Future_, because free online distribution is a crucial part of that film’s form.

As there are always more prohibitively expensive leftist books published by university presses to come, _Inventing the Future_ will continue to be free to watch and download without the requirement of membership in any special website or group of cinephilia.

Though I will continue to experiment with the distribution of my early works and consider this first decade of filmmaking one of joy and lessons learned, I can speak of too many instances of not being paid by universities or art institutions, or too many times when I would receive a speaking fee but no screening fee. I have seen how for many it is not obvious that when a film is accessible for free online it is for people not privileged enough to be in school or an art institution—it isn’t there to be a free addition to a teacher’s curated lesson plan.

In the end, every decision relates to questions of form. If there is a lesson of the cut it is that cinema’s nature is to disappear. Distribution is a form of disappearance. And in the future, I desire my early work to disappear in new ways.

The formula for “content” is: films are made to be programmed. This is the correlationism of contemporary film. Artists have a different formula, brightly obvious: films are made to be seen. Even a great critic like Clement Greenberg had to admit that a breakthrough will always be recognized by another artist first, the true ideal spectator. So an addition: filmmakers make films for other artists to see, not as they please, not under self-selected circumstances, but under circumstances existing already, given and transmitted from the past.

We’ve seen the passage from “this is beautiful”, to “this is art”—and then a passage from “this is art” to “this is content”. To see a passage from “this is content”, to “this is form”, it is a question of artists rethinking distribution, not as a one time break with the nature of things, but a step by step process to adequately think how to change that very nature.