---
link: true
synopsis: During a period of exuberance and uncertainty, a group of artists and critics
  grow into new ideals and ways of thinking, as the old forms are no longer satisfying.
release: 2022-05-01
layout: movie
title: Night is Limpid
director: Isiah Medina
watch: https://youtu.be/mxIhCq4XPyc
image: "/assets/uploads/movies_im_nightislimpid.jpg"

---
![](/assets/uploads/movies_im_nightislimpidposter.png)