---
title: Late Embryo
director: Kelley Dong
link: late-embryo
synopsis: 300 frames of 56 photographs taken over 20 days compressed into the span
  of the time it takes for you to answer my call as I pull up into the driveway and
  wait by the phone.
release: 2017-12-21
image: assets/uploads/Movies_KD_LateEmbryo.png
watch: https://youtu.be/fNLePFSn8kM
layout: movie

---
* [Download link](https://drive.google.com/open?id=1W5bXfQ5tYp1aR8xOzDBoW-2fteuJ9nfZ) for _Late Embryo_ (.mov, 11MB)