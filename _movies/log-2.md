---
link: true
synopsis: From Winnipeg to Paris, from Montreal to the Philippines, the dash between
  "Filipino-Canadian" becomes a minus.
release: 2020-03-11
layout: movie
title: log 2
director: Isiah Medina
watch: https://youtu.be/VWd7llH1iRM
image: "/assets/uploads/Logs_IM_Log2.png"

---
