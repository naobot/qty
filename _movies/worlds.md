---
link: true
synopsis: From this world to the next
release: 2021-07-20
layout: movie
title: Worlds
director: Isaac Goes
watch: https://www.youtube.com/watch?v=EfkQR-4E5kU
image: "/assets/uploads/ig_worlds_5.png"

---
![](/assets/uploads/ig_worlds_poster_small.png)

* [Download link](https://drive.google.com/file/d/18OQezIJYzcSGf-z08LtUohFBkeOiPIJ4) for _Worlds_ (2K H264, 6.5 GB)
* ["Time Enough, and Worlds: Interview with Isaac Goes"](https://quantitycinema.com/2021/07/20/time-enough-and-worlds-interview-with-isaac-goes) by Isiah Medina and Kelley Dong
* [_Worlds_ \[notes + extras\]](http://kinet.media/diopter/worlds-notes-extras) (Kinet Media)