---
title: Inventing the Future
director: Isiah Medina
link: inventing-the-future
synopsis: Take back control over our future and foster the ambition for a world more
  modern than capitalism. Demand full automation, demand a reduced work week, demand
  universal basic income, destroy the work ethic.
release: 2020-03-30
image: "/assets/uploads/movies_im_itf.png"
watch: https://youtu.be/LY44I9P_QZU
layout: movie

---
![](/assets/uploads/movies_im_inventingthefuture1.png)  
Poster design by [Avery Medina](https://averymedina.com/)

* [Download link](https://drive.google.com/file/d/1uIK0IWWp4qpb-1XIECM6KfDCHPW-UAiG/view?usp=sharing) for _Inventing the Future_ (2K H264, 9GB)
* Subtitles
  * [English](https://drive.google.com/file/d/132HIa4ghITA4EfTaXiZj8AAdE4-enOop/view?usp=sharing) (.srt)
  * [Russian](https://drive.google.com/file/d/1dXdq0Tubwei9Uj4yGzU2xVbAqDqXG_9h/view?usp=sharing) (.srt) by Nikita Sazonov
  * [French](https://drive.google.com/file/d/1_1LaQ6dJ741HsrtKozChYc74PX0YqcrS/view?usp=sharing) (.srt) by Alexandre Galmard
  * [Catalan](https://drive.google.com/file/d/1IlBZGjxawboU4uH_TQmibQ6bj8IHqcjs/view?usp=sharing) (.srt) by Albert Vilalta and Pascal Trencia