---
title: Semi-auto Colours
director: Isiah Medina
link: semi-auto-colours
synopsis: 'Kids from the West End learn to count to One. '
release: 2010-04-29
image: assets/uploads/Movies_IM_SemiAuto.png
watch: https://vimeo.com/13987598
layout: movie

---
