---
title: Shooting Star Summer Solstice
director: Kelley Dong
link: shooting-star-summer-solstice
watch: https://www.youtube.com/watch?v=DbC3JzrcAPk
synopsis: 'Alexander von Humboldt climbs a volcano. A parade comes to town. '
release: 2019-07-19
image: assets/uploads/Movies_KD_ShootingStar.png
layout: movie

---
* [Download link](https://drive.google.com/open?id=1ApfRqdSPy8q6--8aX_4WC3nFbsXKdB_b) for _Shooting Star Summer Solstice_ (.mov, 244MB)