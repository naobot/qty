---
title: Breaking and Entering
director: Kelley Dong
link: breaking-and-entering
synopsis: The impossible childhood home, it cannot be reached and it is not the same.
release: 2017-12-21
image: assets/uploads/Movies_KD_BreakingandEntering.png
watch: https://youtu.be/Lh5TL5g7SkQ
layout: movie

---
* [Download link](https://drive.google.com/open?id=1xi56rTs9pQ1Dd2CU6HvK5jbjW-hjdEmH) for _Breaking and Entering_ (.mov, 102MB)