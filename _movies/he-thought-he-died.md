---
layout: movie
title: He Thought He Died
director: Isiah Medina
link: true
release: 2023-09-11
watch: http://vimeo.com/953714214
synopsis: A painter stages a heist to steal his paintings back from the vault of
  a museum. A filmmaker happens to be at the museum on the same day.
image: /assets/uploads/movies_im_hthd.jpg
---
![](/assets/uploads/movies_im_hthd_poster.jpg)

* [T﻿hree Questions: Andrei Pora with Isiah Medina](https://agnes.queensu.ca/digital-publication/he-thought-he-died/?state=read) *(Agnes Etherington Art Centre)*
* [He Thought He Died (2023) 4K](https://www.youtube.com/watch?v=1kx-Sh4f00U) *(Youtube)*